import firebase_admin
import pyrebase
import json
from firebase_admin import credentials, auth

# Firebase Admin Connection
# Note: The file fbadmin.json should not be made public. I've deleted the old key in order to make the repositiory public.
cred = credentials.Certificate('fbadmin.json')
firebase = firebase_admin.initialize_app(cred)
pb = pyrebase.initialize_app(json.load(open('fbconfig.json')))


db = pb.database()